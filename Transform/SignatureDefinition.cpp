#include <string>
#include "llvm/IR/Instruction.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "SignatureDefinition.h"
#include "SyntheticSignatureDefinition.h"
#include "InstructionProxy.h"

using namespace llvm;

extern cl::opt<std::string> MArch;
extern cl::opt<std::string> MCPU;

SignatureDefinition *SignatureDefinition::singleton = nullptr;

SignatureDefinition * SignatureDefinition::getSignatureDefinition(LLVMContext &context) {
  if (SignatureDefinition::singleton == nullptr) {
    switch(SignatureDefinition::getMCU()) {
    case synthetic:
      SignatureDefinition::singleton = new SyntheticSignatureDefinition(context);
      break;
    }
  }
  return SignatureDefinition::singleton;
}

SignatureDefinition::MCU SignatureDefinition::getMCU(void) {
  std::string cpu = MCPU;
  
  // get the cpu to use
  if (MCPU.empty()) {
    // the cpu isn't defined, fall back to the default for the arch
    if (MArch.empty()) {
      // the arch isn't defined, fall back to the global default
      cpu = SignatureDefinition::MCU::synthetic;
    }
  }
}

const Signature * SignatureDefinition::getSignature(const InstructionProxy *inst) {

  if (signatureMap.count(*inst)) {
    return signatureMap.at(*inst);
  }
  
  // if it's not there, log it and try a more generic option
  errs() << "Unknown instruction: (" << inst->toString() << "), trying dereferenced version\n";
  if (signatureMap.count(inst->dereference())) {
    return signatureMap.at(inst->dereference());
  }

  // if it's not there, then just use the average
  errs() << "Falling back to average waveform\n";
  return average;
}

void SignatureDefinition::addWaveform(const std::string &inst, LLVMContext &context, const Waveform *waveform) {
  InstructionProxy proxy(inst, context);
  signatureMap[proxy] = new Signature(waveform);
}

std::vector<InstructionProxy> SignatureDefinition::matchSignature(int startIndex, int endIndex, const Signature *key) {
  std::vector<InstructionProxy> result;

  // loop until all of the indices are filled.
  // The algorithm is greedy, just pick the best for the lowest indices and keep going until it's filled.
  // This can run into very non-optimal situations when the space left is short and not much fits.
  // It can also lead to infite looping if nothing left fits in the space.
  while (startIndex != endIndex) {
    int updateFill = startIndex;
    uint64_t closestScore = std::numeric_limits<uint64_t>::max();
    InstructionProxy best = begin()->first;
    int allowedLength;
    if (endIndex > startIndex) {
      allowedLength = endIndex - startIndex;
    } else {
      allowedLength = (key->size() - startIndex) + endIndex;
    }
  
    for (iterator i = begin(), e = end(); i != e; ++i) {
      if (i->first.isInsertable()) {
        // figure out the last index
        int lastIndex = (i->second->size() + startIndex) % key->size();
        // if the instruction fits
        if (i->second->size() <= allowedLength) {
          // get the distance at the current position
          uint64_t score = i->second->distance(key, startIndex, true);
          // if it's the best so far, keep track
          if (score < closestScore) {
            closestScore = score;
            best = i->first;
            updateFill = lastIndex;
          }
        }
      }
    }
    // store the start index and score for debugging
    best.score = closestScore;
    best.index = startIndex;
    // push the instruction 
    result.push_back(best);
    // update the start index
    startIndex = updateFill;
  }
  
  return result;
}

SignatureDefinition::iterator SignatureDefinition::begin(void) {
  return signatureMap.begin();
}

SignatureDefinition::iterator SignatureDefinition::end(void) {
  return signatureMap.end();
}