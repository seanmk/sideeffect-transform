#include <cstdint>
#include <cmath>
#include <fstream>
#include <string>
#include "Signature.h"
#include "SignatureDefinition.h"
#include "SignatureKey.h"

using namespace llvm;

SignatureKey::SignatureKey(const Waveform *wave) : Signature(wave) {}

void SignatureKey::scale(SignatureDefinition *definition) {
  // do nothing for now
}

int SignatureKey::nextIndex(const Signature *toMatch, int startIndex) const {
  int max = startIndex + waveform->size();
  int closest = 0;
  uint64_t closestScore = std::numeric_limits<uint64_t>::max();

  for (int index = startIndex; index < max; index++) {
    uint64_t score = toMatch->distance(this, index, true);

    // if the value is closest 
    if (score < closestScore) {
      closestScore = score;
      closest = index % waveform->size();
    }
  }
  
  return closest;
}

SignatureKey * SignatureKey::fromFile(std::ifstream &infile) {
  int left, right;
  char delim;
  Waveform *key = new Waveform();
  
  while ((infile >> left >> delim >> right) && (delim == ',')) {
    key->push_back({left, right});
  }
  
  return new SignatureKey(key);
}
