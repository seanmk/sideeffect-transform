#include "Signature.h"
#include "SignatureDefinition.h"
#include "SyntheticSignatureDefinition.h"

using namespace llvm;

SyntheticSignatureDefinition::SyntheticSignatureDefinition(LLVMContext &context) {
  
  addWaveform("add[i16]_i16_i16", context, new Waveform({{66, 0}}));
  
  addWaveform("add[i32]_i32_i32", context, new Waveform({{69, 0}}));
  
  addWaveform("add[i64]_i64_i64", context, new Waveform({{74, 0}}));
  
  addWaveform("add[i8]_i8_i8", context, new Waveform({{65, 0}}));
  
  addWaveform("alloca[i16]_i32", context, new Waveform({{74, 0}}));
  
  addWaveform("alloca[i16*]_i32", context, new Waveform({{88, 0}}));
  
  addWaveform("alloca[i32]_i32", context, new Waveform({{69, 0}}));
  
  addWaveform("alloca[i32*]_i32", context, new Waveform({{74, 0}}));
  
  addWaveform("alloca[i64]_i64", context, new Waveform({{67, 0}}));
  
  addWaveform("alloca[i64*]_i32", context, new Waveform({{74, 0}}));
  
  addWaveform("alloca[i8]_i32", context, new Waveform({{90, 0}}));
  
  addWaveform("alloca[i8*]_i32", context, new Waveform({{82, 0}}));
  
  addWaveform("and[i16]_i16_i16", context, new Waveform({{71, 0}}));
  
  addWaveform("and[i1]_i1_i1", context, new Waveform({{88, 0}}));
  
  addWaveform("and[i32]_i32_i32", context, new Waveform({{82, 0}}));
  
  addWaveform("and[i64]_i64_i64", context, new Waveform({{75, 0}}));
  
  addWaveform("and[i8]_i8_i8", context, new Waveform({{81, 0}}));
  
  addWaveform("ashr[i16]_i16_i16", context, new Waveform({{65, 0}}));
  
  addWaveform("ashr[i32]_i32_i32", context, new Waveform({{78, 0}}));
  
  addWaveform("ashr[i64]_i64_i64", context, new Waveform({{83, 0}}));
  
  addWaveform("ashr[i8]_i8_i8", context, new Waveform({{75, 0}}));
  
  addWaveform("bitcast[i16*]_i16*", context, new Waveform({{85, 0}}));
  
  addWaveform("bitcast[i16*]_i32*", context, new Waveform({{66, 0}}));
  
  addWaveform("bitcast[i16*]_i64*", context, new Waveform({{87, 0}}));
  
  addWaveform("bitcast[i16*]_i8*", context, new Waveform({{88, 0}}));
  
  addWaveform("bitcast[i32*]_i16*", context, new Waveform({{84, 0}}));
  
  addWaveform("bitcast[i32*]_i32*", context, new Waveform({{67, 0}}));
  
  addWaveform("bitcast[i32*]_i64*", context, new Waveform({{65, 0}}));
  
  addWaveform("bitcast[i32*]_i8*", context, new Waveform({{73, 0}}));
  
  addWaveform("bitcast[i64*]_i16*", context, new Waveform({{66, 0}}));
  
  addWaveform("bitcast[i64*]_i32*", context, new Waveform({{84, 0}}));
  
  addWaveform("bitcast[i64*]_i64*", context, new Waveform({{72, 0}}));
  
  addWaveform("bitcast[i64*]_i8*", context, new Waveform({{72, 0}}));
  
  addWaveform("bitcast[i8*]_i16*", context, new Waveform({{87, 0}}));
  
  addWaveform("bitcast[i8*]_i32*", context, new Waveform({{87, 0}}));
  
  addWaveform("bitcast[i8*]_i64*", context, new Waveform({{76, 0}}));
  
  addWaveform("bitcast[i8*]_i8*", context, new Waveform({{67, 0}}));
  
  addWaveform("br[void]_i1_label_label", context, new Waveform({{83, 0}}));
  
  addWaveform("br[void]_label", context, new Waveform({{80, 0}}));
  
  addWaveform("call[i16]", context, new Waveform({{89, 0}}));
  
  addWaveform("call[i32]", context, new Waveform({{70, 0}}));
  
  addWaveform("call[i64]", context, new Waveform({{73, 0}}));
  
  addWaveform("call[i8]", context, new Waveform({{76, 0}}));
  
  addWaveform("getelementptr[i16*]_i16*_i16*", context, new Waveform({{71, 0}}));
  
  addWaveform("getelementptr[i16*]", context, new Waveform({{87, 0}}));
  
  addWaveform("getelementptr[i32*]_i32*_i32*", context, new Waveform({{32, 0}}));
  
  addWaveform("getelementptr[i32*]", context, new Waveform({{84, 0}}));
  
  addWaveform("getelementptr[i64*]_i64*_i64*", context, new Waveform({{83, 0}}));
  
  addWaveform("getelementptr[i64*]", context, new Waveform({{70, 0}}));
  
  addWaveform("getelementptr[i8*]_i8*_i8*", context, new Waveform({{85, 0}}));
  
  addWaveform("getelementptr[i8*]", context, new Waveform({{77, 0}}));
  
  addWaveform("icmp[i1]_i16_i16", context, new Waveform({{68, 0}}));
  
  addWaveform("icmp[i1]_i16*_i16*", context, new Waveform({{88, 0}}));
  
  addWaveform("icmp[i1]_i32_i32", context, new Waveform({{73, 0}}));
  
  addWaveform("icmp[i1]_i32*_i32*", context, new Waveform({{78, 0}}));
  
  addWaveform("icmp[i1]_i64_i64", context, new Waveform({{83, 0}}));
  
  addWaveform("icmp[i1]_i64*_i64*", context, new Waveform({{90, 0}}));
  
  addWaveform("icmp[i1]_i8_i8", context, new Waveform({{89, 0}}));
  
  addWaveform("icmp[i1]_i8*_i8*", context, new Waveform({{85, 0}}));
  
  addWaveform("inttoptr[i16*]_i16", context, new Waveform({{78, 0}}));
  
  addWaveform("inttoptr[i16*]_i8", context, new Waveform({{84, 0}}));
  
  addWaveform("inttoptr[i32*]_i16", context, new Waveform({{82, 0}}));
  
  addWaveform("inttoptr[i32*]_i8", context, new Waveform({{84, 0}}));
  
  addWaveform("inttoptr[i64*]_i16", context, new Waveform({{86, 0}}));
  
  addWaveform("inttoptr[i64*]_i8", context, new Waveform({{89, 0}}));
  
  addWaveform("inttoptr[i8*]_i16", context, new Waveform({{73, 0}}));
  
  addWaveform("inttoptr[i8*]_i8", context, new Waveform({{67, 0}}));
  
  addWaveform("load[i16]_i16*", context, new Waveform({{76, 0}}));
  
  addWaveform("load[i16*]_i16*", context, new Waveform({{80, 0}}));
  
  addWaveform("load[i32]_i32*", context, new Waveform({{78, 0}}));
  
  addWaveform("load[i32*]_i32*", context, new Waveform({{87, 0}}));
  
  addWaveform("load[i64]_i64*", context, new Waveform({{82, 0}}));
  
  addWaveform("load[i64*]_i64*", context, new Waveform({{81, 0}}));
  
  addWaveform("load[i8]_i8*", context, new Waveform({{76, 0}}));
  
  addWaveform("load[i8*]_i8*", context, new Waveform({{85, 0}}));
  
  addWaveform("lshr[i16]_i16_i16", context, new Waveform({{85, 0}}));
  
  addWaveform("lshr[i32]_i32_i32", context, new Waveform({{79, 0}}));
  
  addWaveform("lshr[i64]_i64_i64", context, new Waveform({{83, 0}}));
  
  addWaveform("lshr[i8]_i8_i8", context, new Waveform({{75, 0}}));
  
  addWaveform("mul[i16]_i16_i16", context, new Waveform({{65, 0}}));
  
  addWaveform("mul[i32]_i32_i32", context, new Waveform({{84, 0}}));
  
  addWaveform("mul[i64]_i64_i64", context, new Waveform({{90, 0}}));
  
  addWaveform("mul[i8]_i8_i8", context, new Waveform({{90, 0}}));
  
  addWaveform("or[i16]_i16_i16", context, new Waveform({{66, 0}}));
  
  addWaveform("or[i1]_i1_i1", context, new Waveform({{83, 0}}));
  
  addWaveform("or[i32]_i32_i32", context, new Waveform({{84, 0}}));
  
  addWaveform("or[i64]_i64_i64", context, new Waveform({{81, 0}}));
  
  addWaveform("or[i8]_i8_i8", context, new Waveform({{69, 0}}));
  
  addWaveform("phi[i16]", context, new Waveform({{82, 0}}));
  
  addWaveform("phi[i32]", context, new Waveform({{83, 0}}));
  
  addWaveform("phi[i64]", context, new Waveform({{80, 0}}));
  
  addWaveform("phi[i8]", context, new Waveform({{72, 0}}));
  
  addWaveform("ret[void]_i16", context, new Waveform({{90, 0}}));
  
  addWaveform("ret[void]_i32", context, new Waveform({{77, 0}}));
  
  addWaveform("ret[void]_i64", context, new Waveform({{77, 0}}));
  
  addWaveform("ret[void]_i8", context, new Waveform({{72, 0}}));
  
  addWaveform("sdiv[i16]_i16_i16", context, new Waveform({{72, 0}}));
  
  addWaveform("sdiv[i32]_i32_i32", context, new Waveform({{65, 0}}));
  
  addWaveform("sdiv[i64]_i64_i64", context, new Waveform({{74, 0}}));
  
  addWaveform("sdiv[i8]_i8_i8", context, new Waveform({{73, 0}}));
  
  addWaveform("select[i16]_i1_i16_i16", context, new Waveform({{83, 0}}));
  
  addWaveform("select[i32]_i1_i32_i32", context, new Waveform({{32, 0}}));
  
  addWaveform("select[i64]_i1_i64_i64", context, new Waveform({{86, 0}}));
  
  addWaveform("select[i8]_i1_i8_i8", context, new Waveform({{70, 0}}));
  
  addWaveform("sext[i16]_i1", context, new Waveform({{71, 0}}));
  
  addWaveform("sext[i16]_i8", context, new Waveform({{69, 0}}));
  
  addWaveform("sext[i32]_i16", context, new Waveform({{90, 0}}));
  
  addWaveform("sext[i32]_i1", context, new Waveform({{71, 0}}));
  
  addWaveform("sext[i32]_i8", context, new Waveform({{74, 0}}));
  
  addWaveform("sext[i64]_i16", context, new Waveform({{72, 0}}));
  
  addWaveform("sext[i64]_i1", context, new Waveform({{83, 0}}));
  
  addWaveform("sext[i64]_i32", context, new Waveform({{77, 0}}));
  
  addWaveform("sext[i64]_i8", context, new Waveform({{85, 0}}));
  
  addWaveform("sext[i8]_i1", context, new Waveform({{73, 0}}));
  
  addWaveform("shl[i16]_i16_i16", context, new Waveform({{69, 0}}));
  
  addWaveform("shl[i32]_i32_i32", context, new Waveform({{87, 0}}));
  
  addWaveform("shl[i64]_i64_i64", context, new Waveform({{78, 0}}));
  
  addWaveform("shl[i8]_i8_i8", context, new Waveform({{80, 0}}));
  
  addWaveform("srem[i16]_i16_i16", context, new Waveform({{73, 0}}));
  
  addWaveform("srem[i32]_i32_i32", context, new Waveform({{88, 0}}));
  
  addWaveform("srem[i64]_i64_i64", context, new Waveform({{86, 0}}));
  
  addWaveform("srem[i8]_i8_i8", context, new Waveform({{76, 0}}));
  
  addWaveform("store[void]_i16_i16*", context, new Waveform({{70, 0}}));
  
  addWaveform("store[void]_i16*_i16*", context, new Waveform({{73, 0}}));
  
  addWaveform("store[void]_i32_i32*", context, new Waveform({{88, 0}}));
  
  addWaveform("store[void]_i32*_i32*", context, new Waveform({{88, 0}}));
  
  addWaveform("store[void]_i64_i64*", context, new Waveform({{77, 0}}));
  
  addWaveform("store[void]_i64*_i64*", context, new Waveform({{75, 0}}));
  
  addWaveform("store[void]_i8_i8*", context, new Waveform({{65, 0}}));
  
  addWaveform("store[void]_i8*_i8*", context, new Waveform({{88, 0}}));
  
  addWaveform("sub[i16]_i16_i16", context, new Waveform({{85, 0}}));
  
  addWaveform("sub[i32]_i32_i32", context, new Waveform({{75, 0}}));
  
  addWaveform("sub[i64]_i64_i64", context, new Waveform({{79, 0}}));
  
  addWaveform("sub[i8]_i8_i8", context, new Waveform({{83, 0}}));
  
  addWaveform("switch[void]", context, new Waveform({{83, 0}}));
  
  addWaveform("trunc[i16]_i32", context, new Waveform({{67, 0}}));
  
  addWaveform("trunc[i16]_i64", context, new Waveform({{71, 0}}));
  
  addWaveform("trunc[i32]_i64", context, new Waveform({{73, 0}}));
  
  addWaveform("trunc[i8]_i16", context, new Waveform({{84, 0}}));
  
  addWaveform("trunc[i8]_i32", context, new Waveform({{80, 0}}));
  
  addWaveform("trunc[i8]_i64", context, new Waveform({{73, 0}}));
  
  addWaveform("udiv[i16]_i16_i16", context, new Waveform({{72, 0}}));
  
  addWaveform("udiv[i32]_i32_i32", context, new Waveform({{86, 0}}));
  
  addWaveform("udiv[i64]_i64_i64", context, new Waveform({{79, 0}}));
  
  addWaveform("udiv[i8]_i8_i8", context, new Waveform({{90, 0}}));
  
  addWaveform("urem[i16]_i16_i16", context, new Waveform({{88, 0}}));
  
  addWaveform("urem[i32]_i32_i32", context, new Waveform({{73, 0}}));
  
  addWaveform("urem[i64]_i64_i64", context, new Waveform({{32, 0}}));
  
  addWaveform("urem[i8]_i8_i8", context, new Waveform({{72, 0}}));
  
  addWaveform("xor[i16]_i16_i16", context, new Waveform({{66, 0}}));
  
  addWaveform("xor[i1]_i1_i1", context, new Waveform({{87, 0}}));
  
  addWaveform("xor[i32]_i32_i32", context, new Waveform({{82, 0}}));
  
  addWaveform("xor[i64]_i64_i64", context, new Waveform({{80, 0}}));
  
  addWaveform("xor[i8]_i8_i8", context, new Waveform({{73, 0}}));
  
  addWaveform("zext[i16]_i1", context, new Waveform({{65, 0}}));
  
  addWaveform("zext[i16]_i8", context, new Waveform({{69, 0}}));
  
  addWaveform("zext[i32]_i16", context, new Waveform({{79, 0}}));
  
  addWaveform("zext[i32]_i1", context, new Waveform({{79, 0}}));
  
  addWaveform("zext[i32]_i8", context, new Waveform({{86, 0}}));
  
  addWaveform("zext[i64]_i16", context, new Waveform({{82, 0}}));
  
  addWaveform("zext[i64]_i1", context, new Waveform({{66, 0}}));
  
  addWaveform("zext[i64]_i32", context, new Waveform({{69, 0}}));
  
  addWaveform("zext[i64]_i8", context, new Waveform({{81, 0}}));
  
  addWaveform("zext[i8]_i1", context, new Waveform({{78, 0}}));
  

  average = new Signature(new Waveform({{32, 0}}));
}

unsigned int SyntheticSignatureDefinition::getPointerSize(void) const {
  return 64;
}
