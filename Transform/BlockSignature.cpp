#include <vector>
#include <map>
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "InstructionProxy.h"
#include "BlockSignature.h"
#include "Signature.h"
#include "SignatureKey.h"

using namespace llvm;

std::map<BasicBlock *, BlockSignature *> BlockSignature::blockMap;

BlockSignature * BlockSignature::getBlockSignature(BasicBlock *bb) {
  if (blockMap.count(bb)) {
    return blockMap.at(bb);
  }
  
  BlockSignature *sig = new BlockSignature(bb);
  blockMap[bb] = sig;
  return sig;
}

BlockSignature::BlockSignature(BasicBlock *bb) {
  block = bb;
  context = &bb->getContext();
  startIndex = 0;
  endIndex = 0;

  Instruction *first = block->getFirstNonPHIOrDbg();
  bool start = false;
  for (BasicBlock::iterator i = bb->begin(), e = bb->end();
                            i != e; ++i) {
    Instruction *inst = i;
    if (inst == first) {
      start = true;
    }
    // skipt the PHI or Dbg instructions
    if (start) {
      required.push_back(InstructionProxy(inst));
    }
  }
}

BlockSignature::BlockSignature(Function *parent, std::vector<InstructionProxy> instructions, LLVMContext &cont) {
  block = BasicBlock::Create(cont, "Interstitial", parent);
  context = &cont;
  startIndex = 0;
  endIndex = 0;
  required = instructions;
}

void BlockSignature::insertBetween(BasicBlock *predecessor, BasicBlock *successor) {
  BranchInst *branch = BranchInst::Create(successor);

  InstructionProxy last = required.back();
  if (last.isUnconditionalBranch()) {
    required.pop_back();
    block->getInstList().push_back(branch);
    required.push_back(InstructionProxy(branch));
  }
  InstructionProxy lastProposed = proposed.back();
  if (lastProposed.isUnconditionalBranch()) {
    proposed.pop_back();
    proposed.push_back(InstructionProxy(branch));
  }

  // update PHI nodes
  for (BasicBlock::iterator i = successor->begin(), e = successor->end(); 
                            i != e; ++i) {
    PHINode *phi = dyn_cast<PHINode>(i);
    if (!phi) {
      break;
    }
    int index;
    while ((index = phi->getBasicBlockIndex(predecessor)) >= 0) {
      // set the new incoming edge on the original
      phi->setIncomingBlock(index, block);
    }
  }
  
  // now update the predecessor terminator to point at this block
  TerminatorInst *term = predecessor->getTerminator();
  for (unsigned int termIndex = 0; termIndex < term->getNumSuccessors(); termIndex++) {
    if (successor == term->getSuccessor(termIndex)) {
      term->setSuccessor(termIndex, block);
    }
  }
}

bool BlockSignature::startAt(const SignatureKey *key, int start) {
  SignatureDefinition *definition = SignatureDefinition::getSignatureDefinition(*context);
  startIndex = start;
  int index = start;
  proposed.clear();

  for (InstructionProxy inst : required) {
    const Signature *sig = definition->getSignature(&inst);
    // what's the best index at which to put this instruction
    int next = key->nextIndex(sig, index);
    //errs() << inst.toString() << "[" << sig->getWaveform()->at(0).first << "]" << " : " << index << "->" << next << "\n";

    // now try to fill in the instructions up to that index
    std::vector<InstructionProxy> matched = definition->matchSignature(index, next, key);
    //errs() << "matched length " << matched.size() << "\n";
    // concatenate the results
    proposed.insert(proposed.end(), matched.begin(), matched.end());

    // set the index for debugging
    inst.index = next;
    // finally, push the required instruction so that it's after all the stuff we generated to go before it
    proposed.push_back(inst);
    // remember this?  update the index to the end of the present instruction.
    index = (next + sig->size()) % key->size();
  }
  
  // add an interstitial block for any successors if they have other predecessors
  // and this block doesn't end with an unconditional branch
  bool interstitialChanged = false;
  if (!required.back().isUnconditionalBranch()) {
    for (succ_iterator i = succ_begin(block), e = succ_end(block); i!= e; ++i) {
      BasicBlock *successor = *i;
      // check it for other predecessors
      BlockSignature *succSig = getBlockSignature(successor);
      if (!succSig->singlePredecessor()) {
        // if an interstitial block already exists, get that
        BlockSignature *between;
        if (interstitial.count(successor)) {
          between = interstitial.at(successor);
        } else {
          // otherwise, create one and insert it
          between = new BlockSignature(block->getParent(), {InstructionProxy::getUnconditionalBranch(*context)}, *context);
          interstitial[successor] = between;
        }
        // NOW ADJUST IT BOLDFACE
        interstitialChanged = between->startAt(key, (index + 1) % key->size());
      }
    }
  }
  
  // if no change to the final index, then just report that nothing is updated
  if (index == endIndex && !interstitialChanged) {
    return false;
  }
  
  errs() << block << " updated to " << index;
  if (interstitialChanged) {
    errs() << " and interstitial block updated";
  }
  errs() << "\n";
  
  // otherwise, update the key
  endIndex = index;
  return true;
}

int BlockSignature::merge(const SignatureKey *key) const {
  int result = 0;
  int lowestScore = std::numeric_limits<int>::max();
  
  // find the index which requires the least amount of adjustment from other successors
  // so if the key is length 20, and succ0 ends at 3 and succ1 ends at 18, 
  // succ0's score is (20-18)+(3-0)=5 and succ1's score is (18-3)=15
  // The lower score is chosen so the result would be 3
  for (pred_iterator i = pred_begin(block), e = pred_end(block); i != e; ++i) {
    BasicBlock *predecessor = *i;
    BlockSignature *predSig = getBlockSignature(predecessor);

    // this includes the possibility that an interstitial block may exist
    int index = predSig->getEnd(block);
    int score = 0;

    for (pred_iterator j = pred_begin(block); j != e; ++j) {
      BasicBlock *other = *j;
      BlockSignature *otherSig = getBlockSignature(other);
      
      score += otherSig->getDifference(block, index, key);
    }

    if (score < lowestScore) {
      result = index;
      lowestScore = score;
    }
  }
  
  return result;
}

bool BlockSignature::validate(const SignatureKey *key) {
  bool accept = true;
  // just verify that there are no gaps (there shouldn't be)
  for (succ_iterator i = succ_begin(block), e = succ_end(block); i!= e; ++i) {
    BasicBlock *successor = *i;
    BlockSignature *succSig = getBlockSignature(successor);
    
    int difference = getDifference(successor, succSig->getStart(), key);
    if (difference > 1) {
      errs() << "Has a successor gap! : " << difference << "\n";
      accept = false;
    }
  }
  
  return accept;
}

void BlockSignature::apply(void) {
  int requiredIndex = 0;

  if (required.size() > 0) {
    Instruction *nextRequired = required.at(requiredIndex).getInstruction();
    
    for (InstructionProxy proxy : proposed) {
      Instruction *inst = proxy.getInstruction();
      if (inst != nextRequired) {
        // insert it
        block->getInstList().insert(nextRequired, inst);
      } else {
        // get the next required instruction
        requiredIndex++;
        if (required.size() > requiredIndex) {
          nextRequired = required.at(requiredIndex).getInstruction();
        }
      }
    }
    
    // add the interstitial blocks
    if (!interstitial.empty()) {
      if (!nextRequired->isTerminator()) {
        errs() << "Block ended with a non-terminator somehow!\n";
      }
      TerminatorInst *terminator = cast<TerminatorInst>(nextRequired);
      
      for (unsigned int i = 0; i < terminator->getNumSuccessors(); i++) {
        BasicBlock *successor = terminator->getSuccessor(i);
        // if there is an interstitial block to insert
        if (interstitial.count(successor)) {
          BlockSignature *insert = interstitial.at(successor);
          // add the branch and update the blocks
          insert->insertBetween(block, successor);
          // add the other instructions
          insert->apply();
        }
      }
    }
  }
}

int BlockSignature::getStart(void) const {
  return startIndex;
}

int BlockSignature::getEnd(const BasicBlock *successor) const {
  if (interstitial.count(successor)) {
    return interstitial.at(successor)->getEnd(successor);
  }
  return endIndex;
}

int BlockSignature::getDifference(const BasicBlock *successor, int nextIndex, const SignatureKey *key) const {
  int end = getEnd(successor);
  if (nextIndex > end) {
    return nextIndex - end;
  } else {
    return (key->size() - end) + nextIndex;
  }
}

bool BlockSignature::singlePredecessor(void) const {
  return block->getSinglePredecessor() != nullptr;
}

bool BlockSignature::singleSuccessor(void) const {
  const TerminatorInst *term = block->getTerminator();
  return term->getNumSuccessors() == 1;
}

void BlockSignature::print(raw_ostream &stream) const {
  stream << block->getName() << "(" << block << ") : " << proposed.size() << "(" << required.size() << ")";
  for (std::map<const BasicBlock *, BlockSignature *>::const_iterator i = interstitial.begin(), e= interstitial.end(); i != e; ++i) {
    std::pair<const BasicBlock *, BlockSignature *> pair = *i;
    stream << ", " << pair.second->proposed.size();
  }
  stream << "\n";
}
