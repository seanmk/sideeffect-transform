#ifndef __INCLUDE_SIG_KEY__
#define __INCLUDE_SIG_KEY__

#include <vector>
#include <string>
#include "llvm/IR/BasicBlock.h"

#include "Signature.h"
#include "SignatureDefinition.h"

namespace llvm {
  class SignatureKey : public Signature {
  public:
    SignatureKey(const Waveform *wave);
    void scale(SignatureDefinition *definition);
    int nextIndex(const Signature *toMatch, int startIndex) const;

    static SignatureKey * fromFile(std::ifstream &infile);
  };
}

#endif