#ifndef __INCLUDE_SYNTHETIC_SIGNATURE_DEFINITION__
#define __INCLUDE_SYNTHETIC_SIGNATURE_DEFINITION__

#include "SignatureDefinition.h"

namespace llvm {
  class SyntheticSignatureDefinition : public SignatureDefinition {
  public:
    SyntheticSignatureDefinition(LLVMContext &context);
    virtual unsigned int getPointerSize(void) const;
  };
}

#endif