#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>

#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "BlockSignature.h"
#include "SignatureKey.h"

using namespace llvm;

cl::opt<std::string> KeyFilename("keyfile", cl::desc("File containing side-effect signature to apply"), cl::value_desc("filename"));

namespace {
  struct TransformSE : public ModulePass {
    static char ID;
    TransformSE() : ModulePass(ID) {}
    
    bool runOnModule(Module &module) override {
      errs() << "Transforming Side-effects...\n";
      std::srand(std::time(0));
      
      if (KeyFilename.length() == 0) {
        errs() << "Must specify <keyfile> argument!\n";
        return false;
      }
      
      std::ifstream keyfile(KeyFilename.c_str());
      SignatureKey *key = SignatureKey::fromFile(keyfile);

      // the quick brown fox jumps over the lazy dog
      //SignatureKey *key = new SignatureKey(new Waveform({{84, 0},{72, 0},{69, 0},{32, 0},{81, 0},{85, 0},{73, 0},{67, 0},{75, 0},{32, 0},{66, 0},{82, 0},{79, 0},{87, 0},{78, 0},{32, 0},{70, 0},{79, 0},{88, 0},{32, 0},{74, 0},{85, 0},{77, 0},{80, 0},{83, 0},{32, 0},{79, 0},{86, 0},{69, 0},{82, 0},{32, 0},{84, 0},{72, 0},{69, 0},{32, 0},{76, 0},{65, 0},{90, 0},{89, 0},{32, 0},{68, 0},{79, 0},{71, 0}}));
      // a list of all the blocks
      std::vector<BlockSignature *> blocks;
      // get the signature definition
      SignatureDefinition *signature = SignatureDefinition::getSignatureDefinition(module.getContext());
    
      // first loop through all the functions in the module
      for (Module::iterator mi = module.begin(), mend = module.end();
                            mi != mend; ++mi) {
        Function &function = *mi;
        
        // now loop through all the basic blocks in the function
        for (Function::iterator fi = function.begin(), fend = function.end();
                                fi != fend; ++fi) {
          BasicBlock *block = fi;
          blocks.push_back(BlockSignature::getBlockSignature(block));
        }
      }
      
      // scale the key
      key->scale(signature);
      
      // iterate to a fixed point
      while (true) {
        errs() << "Starting instruction alignment pass...\n";
        bool changed = false;

        for (BlockSignature *block : blocks) {            
          // get the index from merging the predecessors
          int lastIndex = block->merge(key);
          // update the basic block final index
          changed = block->startAt(key, (lastIndex + 1) % key->size()) || changed;
        }
        
        // break out if nothing updated
        if (!changed) {
          break;
        }
      }
      
      // validate that everything worked okay, then apply the changes
      for (BlockSignature *block : blocks) {
        block->validate(key);
        block->apply();
      }
      
      for (BlockSignature *block : blocks) {
        block->print(errs()); 
      }

      errs() << "done\n";
      
      return true;
    }
    
  };
}

char TransformSE::ID = 0;
static RegisterPass<TransformSE> X("transform-sideeffects", "Add synthetic side-effects to a program", false, false);
