#ifndef __INCLUDE_BLK_POS__
#define __INCLUDE_BLK_POS__

#include <vector>
#include <map>
#include "llvm/IR/BasicBlock.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "InstructionProxy.h"
#include "SignatureKey.h"

namespace llvm {
  class BlockSignature {
  public:
    static BlockSignature * getBlockSignature(BasicBlock *bb);
    void insertBetween(BasicBlock *predecessor, BasicBlock *successor);
    bool startAt(const SignatureKey *key, int start);
    int merge(const SignatureKey *key) const;
    bool validate(const SignatureKey *key);
    void apply(void);
    int getStart(void) const;
    int getEnd(const BasicBlock *successor) const;
    int getDifference(const BasicBlock *successor, int nextIndex, const SignatureKey *key) const;
    bool singlePredecessor(void) const;
    bool singleSuccessor(void) const;
    void print(raw_ostream &stream) const;

  private:
    BlockSignature(BasicBlock *bb);
    BlockSignature(Function *parent, std::vector<InstructionProxy> instructions, LLVMContext &cont);
    BasicBlock *block;
    LLVMContext *context;
    std::vector<InstructionProxy> required;
    std::vector<InstructionProxy> proposed;
    int startIndex;
    int endIndex;
    std::map<const BasicBlock *, BlockSignature *> interstitial;
    static std::map<BasicBlock *, BlockSignature *> blockMap;
  };
}

#endif
