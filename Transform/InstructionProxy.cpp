#include <cstdlib>
#include <string>
#include <set>

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Metadata.h"

#include "InstructionProxy.h"

using namespace llvm;

std::set<std::string> InstructionProxy::ignoreOperands = {"call", "getelementptr", "phi", "switch"};
std::set<std::string> InstructionProxy::nonInsertable = {"call", "phi", "switch", "br", "ret", "store", "getelementptr", "load"};

InstructionProxy::InstructionProxy(Instruction *inst) {
  instruction = inst;
  
  operation = inst->getOpcodeName();
  type = inst->getType();
  context = &instruction->getParent()->getContext();
  if (!InstructionProxy::ignoreOperands.count(operation)) {
    for (User::const_op_iterator i = inst->op_begin(), e = inst->op_end();
                                 i != e; ++i) {
      const Value *value = i->get();
      operands.push_back(value->getType());
    }
  }
  index = 0;
}

InstructionProxy::InstructionProxy(const std::string &desc, LLVMContext &cont) {
  instruction = nullptr;
  std::size_t lbracket = desc.find('[');
  std::size_t rbracket = desc.find(']');
  // doing unsafe things because we assume we'll find our needles...
  operation = desc.substr(0, lbracket);
  std::string typeName = desc.substr(lbracket + 1, (rbracket - lbracket) - 1);
  // some instruction imply pointers
  if (operation == "alloca") {
    typeName += "*";
  }
  type = getTypeFromString(typeName, cont);
  context = &cont;
  
  std::size_t next = desc.find('_', rbracket);
  while (next != std::string::npos) {
    std::size_t last = next + 1;
    next = desc.find('_', last);
    if (next != std::string::npos) {
      typeName = desc.substr(last, next - last);
    } else {
      typeName = desc.substr(last);
    }
    operands.push_back(getTypeFromString(typeName, cont));
  }
  index = 0;
}

InstructionProxy InstructionProxy::dereference(void) const {
  InstructionProxy deref(*this);
  if (deref.type->isPointerTy()) {
    deref.type = Type::getIntNTy(*context, POINTER_WIDTH);
  }
  for (int i = 0; i < deref.operands.size(); i++) {
    if (deref.operands.at(i)->isPointerTy()) {
      deref.operands[i] = Type::getIntNTy(*context, POINTER_WIDTH);
    }
  }
    
  return deref;
}

std::string InstructionProxy::toString(void) const {
  std::string result = operation + "[" + getStringFromType(type) + "]";
  for (const Type *op : operands) {
    result += "_" + getStringFromType(op);
  }
  return result;
}

Instruction * InstructionProxy::getInstruction(void) {
  MDNode *metaIndex = MDNode::get(*context, MDString::get(*context, std::to_string(index)));
  MDNode *metaScore = MDNode::get(*context, MDString::get(*context, std::to_string(score)));
  if (instruction != nullptr) {
    instruction->setMetadata(METADATA_INDEX, metaIndex);
    return instruction;
  }
  
  // we have to generate the instruction
  // br is a special case
  Type *types[operands.size()];
  Value *values[operands.size()];
  Instruction *inst;

  // first set up the operands
  for (int i = 0; i < operands.size(); i++) {
    types[i] = operands.at(i);
    if (types[i]->isIntegerTy()) {
      values[i] = ConstantInt::get(types[i], 42);
    } else if (types[i]->isPointerTy()) {
      Constant *intval = ConstantInt::get(types[i]->getPointerElementType(), 42);
      values[i] = ConstantExpr::getIntToPtr(intval, types[i]);
    } // we aren't actually using floats at the moment and labels/void should be handled
  }
  
  
  if (operation == "add") {  
    // add
    inst = BinaryOperator::Create(Instruction::Add, values[0], values[1], "gen");

  } else if (operation == "alloca") {
    // alloca
    inst = new AllocaInst(type, values[0], "gen");

  } else if (operation == "and") {
    // and
    inst = BinaryOperator::Create(Instruction::And, values[0], values[1], "gen");
  
  } else if (operation == "ashr") {
    // ashr
    inst = BinaryOperator::Create(Instruction::AShr, values[0], values[1], "gen");
  
  } else if (operation == "bitcast") {
    // bitcast
    inst = CastInst::Create(Instruction::BitCast, values[0], type, "gen");

  } else if (operation == "icmp") {    
    // icmp
    inst = new ICmpInst(CmpInst::Predicate::ICMP_EQ, values[0], values[1], "gen");
    
  } else if (operation == "inttoptr") {
    // inttoptr
    inst = CastInst::Create(Instruction::IntToPtr, values[0], type, "gen");
    
  } else if (operation == "load") {    
    // load
    inst = new LoadInst(values[0], "gen", true); // volatile = true
    
  } else if (operation == "lshr") {
    // lshr
    inst = BinaryOperator::Create(Instruction::LShr, values[0], values[1], "gen");

  } else if (operation == "mul") {
    // mul
    inst = BinaryOperator::Create(Instruction::Mul, values[0], values[1], "gen");
    
  } else if (operation == "or") {
    // or
    inst = BinaryOperator::Create(Instruction::Or, values[0], values[1], "gen");

  } else if (operation == "sdiv") {
    // sdiv
    inst = BinaryOperator::Create(Instruction::SDiv, values[0], values[1], "gen");
  
  } else if (operation == "select") {
    // select
    inst = SelectInst::Create(values[0], values[1], values[2], "gen");
    
  } else if (operation == "sext") {
    // sext
    inst = CastInst::Create(Instruction::SExt, values[0], type, "gen");
    
  } else if (operation == "shl") {
    // shl
    inst = BinaryOperator::Create(Instruction::Shl, values[0], values[1], "gen");

  } else if (operation == "srem") {    
    // srem
    inst = BinaryOperator::Create(Instruction::SRem, values[0], values[1], "gen");
    
  } else if (operation == "sub") {
    // sub
    inst = BinaryOperator::Create(Instruction::Sub, values[0], values[1], "gen");

  } else if (operation == "trunc") {
    // trunc
    inst = CastInst::Create(Instruction::Trunc, values[0], type, "gen");

  } else if (operation == "udiv") {    
    // udiv
    inst = BinaryOperator::Create(Instruction::UDiv, values[0], values[1], "gen");
    
  } else if (operation == "urem") {
    // urem
    inst = BinaryOperator::Create(Instruction::URem, values[0], values[1], "gen");
  
  } else if (operation == "xor") {
    // xor
    inst = BinaryOperator::Create(Instruction::Xor, values[0], values[1], "gen");
  
  } else if (operation == "zext") {
    // zext
    inst = CastInst::Create(Instruction::ZExt, values[0], type, "gen");
  } else {
    errs() << "Unknown operation " << operation << "\n";
  }
  
  inst->setMetadata(METADATA_INDEX, metaIndex);
  inst->setMetadata(METADATA_SCORE, metaScore);
  return inst;
}

bool InstructionProxy::isInsertable(void) const {
  // can't insert an existing instruction
  if (instruction != nullptr) {
    return false;
  }
  
  return !(InstructionProxy::nonInsertable.count(operation));
}


bool InstructionProxy::operator<(const InstructionProxy &toCompare) const {
  return toString() < toCompare.toString();
}

bool InstructionProxy::isUnconditionalBranch(void) const {
  // string comparisons are easy and slow!
  return toString() == UNCOND_BRANCH_STRING;
}

InstructionProxy InstructionProxy::getUnconditionalBranch(LLVMContext &context) {
  return InstructionProxy(UNCOND_BRANCH_STRING, context);
}

Type * InstructionProxy::getTypeFromString(std::string &typeName, LLVMContext &context) {
  Type *type;
  bool pointer = false;
  if (typeName.find("*") != std::string::npos) {
    pointer = true;
  }

  if (typeName.substr(0, 1) == "i") {
    type = pointer ? (Type *)Type::getIntNPtrTy(context, std::stoi(typeName.substr(1), nullptr)) : (Type *)Type::getIntNTy(context, std::stoi(typeName.substr(1), nullptr));
  } else if (typeName.substr(0, 5) == "float") {
    type = pointer? (Type *)Type::getFloatPtrTy(context) : (Type *)Type::getFloatTy(context);
  } else if (typeName.substr(0, 5) == "label") {
    type = Type::getLabelTy(context);
  } else if (typeName.substr(0, 4) == "void") {
    type = pointer? PointerType::getUnqual(Type::getIntNTy(context, POINTER_WIDTH)) : Type::getVoidTy(context);
  } else {
    // unknown types will just be pointers to the pointer size
    //errs() << "Unknown type string [" << typeName << "]\n";
    type = PointerType::getUnqual(Type::getIntNTy(context, POINTER_WIDTH));
  }
  return type;
}


std::string InstructionProxy::getStringFromType(const Type *type) {
  std::string typeStr;

  if (type->isIntegerTy()) {
    typeStr = "i" + std::to_string(type->getIntegerBitWidth());
  } else if (type->isFloatingPointTy()) {
    typeStr = "float";
  } else if (type->isLabelTy()) {
    typeStr = "label";
  } else if (type->isPointerTy()) {
    const Type *temp = type;
    while (temp != nullptr && temp->isPointerTy()) {
      temp = temp->getPointerElementType();
    }
    if (temp != nullptr && temp->isIntegerTy()) {
      typeStr = "i" + std::to_string(temp->getIntegerBitWidth()) + "*";
    } else {
      typeStr = "i" + std::to_string(POINTER_WIDTH) + "*";
    }
  } else if (type->isVoidTy()) {
    typeStr = "void";
  } else {
    //errs() << "Unknown type object [";
    //type->print(errs());
    //errs() << "]\n";
    // we use the pointer size for unknown types

    typeStr = "i" + std::to_string(POINTER_WIDTH);
  }
  return typeStr;
}

