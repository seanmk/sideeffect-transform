#include <cstdint>
#include <vector>

#include "Signature.h"
#include "llvm/Support/raw_ostream.h" // errs()


using namespace llvm;

Signature::Signature(const Waveform *wave) {
  waveform = wave;
}

const Waveform * Signature::getWaveform(void) const {
  return waveform;
}

int Signature::size(void) const {
  return waveform->size();
}

uint64_t Signature::distance(const Signature *compare, int offset, bool wrap) const {
  uint64_t score = 0;
  const Waveform *compareWave = compare->getWaveform();
  
  for (unsigned int i = 0; i < waveform->size(); i++) {
    uint64_t left = waveform->at(i).first;
    unsigned int compareIndex = (i + offset);
    if (!wrap && compareIndex >= compare->size()) {
      break;
    } else {
      compareIndex = compareIndex % compare->size();
    }
    uint64_t right = compareWave->at(compareIndex).first;
    score += (left - right) * (left - right);
  }
  return score;
}
