#include <string>
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"

#include "GenerateInstructions.h"

using namespace llvm;

Instruction * GenerateInstructions::generate(std::string code) {
  LLVMContext &context = getGlobalContext();
  Constant *bit = ConstantInt::getFalse(context);
  Constant *left = ConstantInt::get(Type::getInt8Ty(context), 8);
  Constant *right = ConstantInt::get(Type::getInt8Ty(context), 16);
  Constant *word = ConstantInt::get(Type::getInt16Ty(context), 32);
  Value *ptr = ConstantExpr::getIntToPtr(left, Type::getInt8PtrTy(context)); 
  Instruction *inst;

  if (code == "add") {  
    // add
    inst = BinaryOperator::Create(Instruction::Add, left, right, "gen");

  } else if (code == "alloca") {
    // alloca
    inst = new AllocaInst(Type::getInt8Ty(context), right, "gen");

  } else if (code == "and") {
    // and
    inst = BinaryOperator::Create(Instruction::And, left, right, "gen");
  
  } else if (code == "ashr") {
    // ashr
    inst = BinaryOperator::Create(Instruction::AShr, left, right, "gen");
  
  } else if (code == "bitcast") {
    // bitcast
    inst = CastInst::Create(Instruction::BitCast, right, Type::getInt8Ty(context), "gen");

  } else if (code == "icmp") {    
    // icmp
    inst = new ICmpInst(CmpInst::Predicate::ICMP_EQ, left, right, "gen");
    
  } else if (code == "inttoptr") {
    // inttoptr
    inst = CastInst::Create(Instruction::IntToPtr, right, Type::getInt8PtrTy(context), "gen");
    
  } else if (code == "load") {    
    // load
    inst = new LoadInst(ptr, "gen", true); // volatile = true
    
  } else if (code == "lshr") {
    // lshr
    inst = BinaryOperator::Create(Instruction::LShr, left, right, "gen");

  } else if (code == "mul") {
    // mul
    inst = BinaryOperator::Create(Instruction::Mul, left, right, "gen");
    
  } else if (code == "or") {
    // or
    inst = BinaryOperator::Create(Instruction::Or, left, right, "gen");

  } else if (code == "sdiv") {
    // sdiv
    inst = BinaryOperator::Create(Instruction::SDiv, left, right, "gen");
  
  } else if (code == "select") {
    // select
    inst = SelectInst::Create(bit, left, right, "gen");
    
  } else if (code == "sext") {
    // sext
    inst = CastInst::Create(Instruction::SExt, right, Type::getInt16Ty(context), "gen");
    
  } else if (code == "shl") {
    // shl
    inst = BinaryOperator::Create(Instruction::Shl, left, right, "gen");

  } else if (code == "srem") {    
    // srem
    inst = BinaryOperator::Create(Instruction::SRem, left, right, "gen");
    
  } else if (code == "sub") {
    // sub
    inst = BinaryOperator::Create(Instruction::Sub, left, right, "gen");

  } else if (code == "trunc") {
    // trunc
    inst = CastInst::Create(Instruction::Trunc, word, Type::getInt8Ty(context), "gen");

  } else if (code == "udiv") {    
    // udiv
    inst = BinaryOperator::Create(Instruction::UDiv, left, right, "gen");
    
  } else if (code == "urem") {
    // urem
    inst = BinaryOperator::Create(Instruction::URem, left, right, "gen");
  
  } else if (code == "xor") {
    // xor
    inst = BinaryOperator::Create(Instruction::Xor, left, right, "gen");
  
  } else if (code == "zext") {
    // zext
    inst = CastInst::Create(Instruction::ZExt, right, Type::getInt16Ty(context), "gen");
  } else {
    // error
  }

  return inst;
}