#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "GenerateInstructions.h"

using namespace llvm;

namespace {
  struct TestGen : public ModulePass {
    static char ID;

    TestGen() : ModulePass(ID) {}
    
    bool runOnModule(Module &module) override {
      errs() << "Test generation...\n";
      
      // first loop through all the functions in the module
      for (Module::iterator mi = module.begin(), mend = module.end();
                            mi != mend; ++mi) {
        Function &function = *mi;
        
        // now loop through all the basic blocks in the function
        for (Function::iterator fi = function.begin(), fend = function.end();
                                fi != fend; ++fi) {
          BasicBlock &block = *fi;
          
          Instruction *first = block.getFirstNonPHIOrDbg();
          //Value *left = ConstantInt::get(Type::getInt8Ty(module.getContext()), 8);
          //Value *right = ConstantInt::get(Type::getInt8Ty(module.getContext()), 16);
          
          //BinaryOperator *inst = BinaryOperator::Create(Instruction::Add, left, right, "gen");
          Instruction *inst = GenerateInstructions::generate("add");

          block.getInstList().insert(first, inst);
        }

      }

      return true;
    }
  };
}

char TestGen::ID = 0;
static RegisterPass<TestGen> X("testgen", "Test generating instructions", false, false);
