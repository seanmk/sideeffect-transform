#ifndef __INCLUDE_GENERATE_INST__
#define __INCLUDE_GENERATE_INST__

#include <string>
#include "llvm/IR/Instruction.h"

namespace llvm {
  class GenerateInstructions {
  public:
    static Instruction * generate(std::string code);
  };
}

#endif