#ifndef __INCLUDE_PRINTF_INST__
#define __INCLUDE_PRINTF_INST__

namespace llvm {
  void printfInstrument(Instruction *inst, StringRef *string);
}

#endif