#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h" // errs()

#include "InstructionProxy.h"
#include "Signature.h"
#include "SignatureDefinition.h"
#include "PrintfInstrument.h"

using namespace llvm;

namespace {
  struct ApplySyntheticSE : public ModulePass {
    static char ID;

    ApplySyntheticSE() : ModulePass(ID) {}
    
    bool runOnModule(Module &module) override {
      errs() << "Applying Synthetic Side-effect...\n";
      
      // get the signature definition
      SignatureDefinition *definition = SignatureDefinition::getSignatureDefinition(module.getContext());
    
      // first loop through all the functions in the module
      for (Module::iterator mi = module.begin(), mend = module.end();
                            mi != mend; ++mi) {
        Function &function = *mi;
        
        // now loop through all the basic blocks in the function
        for (Function::iterator fi = function.begin(), fend = function.end();
                                fi != fend; ++fi) {
          BasicBlock &block = *fi;
          
          Instruction *first = block.getFirstNonPHIOrDbg();
          bool doInstrument = false;

          for (BasicBlock::iterator bi = block.begin(), bend = block.end();
                                    bi != bend; ++bi) {
            Instruction &inst = *bi;
            if (first == &inst) {
              doInstrument = true;
            }
            
            if (doInstrument) {
              InstructionProxy proxy(&inst);
              const Signature *signature = definition->getSignature(&proxy);
              const Waveform *waveform = signature->getWaveform();
              char letter = (char)waveform->at(0).first;
              StringRef str(&letter, 1);

              printfInstrument(&inst, &str);
            }
          }
        }
      }

      return true;
    }
  };
}

char ApplySyntheticSE::ID = 0;
static RegisterPass<ApplySyntheticSE> X("apply-synthetic", "Add synthetic side-effects to a program", false, false);
