#include "llvm/ADT/StringRef.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"

#include "PrintfInstrument.h"

using namespace llvm;

void llvm::printfInstrument(Instruction *inst, StringRef *string) {
  Module *module = inst->getParent()->getParent()->getParent();

  /***********************************************************
   * Set up the printf function
   */
  PointerType* PointerTy_3 = PointerType::get(IntegerType::get(module->getContext(), 8), 0);
 
  std::vector<Type*>FuncTy_5_args;
  FuncTy_5_args.push_back(PointerTy_3);
  FunctionType* FuncTy_5 = FunctionType::get(
    /*Result=*/IntegerType::get(module->getContext(), 32),
    /*Params=*/FuncTy_5_args,
    /*isVarArg=*/true);
 
  Function* func_printf = module->getFunction("printf");
  if (!func_printf) {
    func_printf = Function::Create(
      /*Type=*/FuncTy_5,
      /*Linkage=*/GlobalValue::ExternalLinkage,
      /*Name=*/"printf", module); // (external, no body)
    func_printf->setCallingConv(CallingConv::C);
  }
 
  AttributeSet func_printf_PAL;
  {
    SmallVector<AttributeSet, 4> Attrs;
    AttributeSet PAS;
    {
      AttrBuilder B;
      B.addAttribute(Attribute::ReadOnly);
      B.addAttribute(Attribute::NoCapture);
      PAS = AttributeSet::get(module->getContext(), 1U, B);
    }
  
    Attrs.push_back(PAS);
    {
      AttrBuilder B;
      B.addAttribute(Attribute::NoUnwind);
      PAS = AttributeSet::get(module->getContext(), ~0U, B);
    }
    Attrs.push_back(PAS);
    func_printf_PAL = AttributeSet::get(module->getContext(), Attrs);
  }
  func_printf->setAttributes(func_printf_PAL);

  /**
   * Done setting up printf
   *************************************************/

  ArrayType* ArrayTy_0 = ArrayType::get(IntegerType::get(module->getContext(), 8), string->size() + 1);
  Constant *const_array_6 = ConstantDataArray::getString(module->getContext(), *string, true);

  GlobalVariable* gvar_array__str = new GlobalVariable(/*Module=*/*module, 
    /*Type=*/ArrayTy_0,
    /*isConstant=*/true,
    /*Linkage=*/GlobalValue::PrivateLinkage,
    /*Initializer=*/0, // has initializer, specified below
    /*Name=*/".str");
  gvar_array__str->setAlignment(1);
  gvar_array__str->setInitializer(const_array_6);

  // Constant Definitions
  std::vector<Constant*> const_ptr_7_indices;
  ConstantInt* const_int32_8 = ConstantInt::get(module->getContext(), APInt(32, 0));
  const_ptr_7_indices.push_back(const_int32_8);
  const_ptr_7_indices.push_back(const_int32_8);
  Constant* const_ptr_7 = ConstantExpr::getGetElementPtr(gvar_array__str, const_ptr_7_indices);

  // Block entry
  CallInst* int32_call = CallInst::Create(func_printf, const_ptr_7, "call", inst);
  int32_call->setCallingConv(CallingConv::C);
  int32_call->setTailCall(true);
  AttributeSet int32_call_PAL;
  {
    SmallVector<AttributeSet, 4> Attrs;
    AttributeSet PAS;
    {
      AttrBuilder B;
      B.addAttribute(Attribute::NoUnwind);
      PAS = AttributeSet::get(module->getContext(), ~0U, B);
    }
  
    Attrs.push_back(PAS);
    int32_call_PAL = AttributeSet::get(module->getContext(), Attrs);
  }
  int32_call->setAttributes(int32_call_PAL);
}
