#ifndef __INCLUDE_SIGNATURE__
#define __INCLUDE_SIGNATURE__

#include <cstdint>
#include <vector>

namespace llvm {
  typedef std::vector<std::pair<const uint64_t, const uint64_t> > Waveform;
  
  class Signature {
  public:
    Signature(const Waveform *wave);
    int size(void) const;
    uint64_t distance(const Signature *compare, int offset, bool wrap) const;
    const Waveform * getWaveform(void) const;
  protected:
    const Waveform *waveform;
  };
}

#endif
