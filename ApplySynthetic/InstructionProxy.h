#ifndef __INCLUDE_INSTRUCTION_PROXY__
#define __INCLUDE_INSTRUCTION_PROXY__

#include <string>
#include <set>
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h" // errs()

#define POINTER_WIDTH 64
#define UNCOND_BRANCH_STRING "br[void]_label"

namespace llvm {
  class InstructionProxy {
  public:
    InstructionProxy(Instruction *inst);
    InstructionProxy(const std::string &desc, LLVMContext &cont);
    InstructionProxy dereference(void) const;
    std::string toString(void) const;
    Instruction * getInstruction(void);
    bool isInsertable(void) const;
    bool operator<(const InstructionProxy &toCompare) const;
    bool isUnconditionalBranch(void) const;
    static InstructionProxy getUnconditionalBranch(LLVMContext &cont);


  private:
    Instruction *instruction;
    std::string operation;
    Type *type;
    std::vector<Type *> operands;
    LLVMContext *context;
    static std::set<std::string> ignoreOperands;
    static std::set<std::string> nonInsertable;
    static Type * getTypeFromString(std::string &typeName, LLVMContext &context);
    static std::string getStringFromType(const Type *type);
  };
}

#endif
