#ifndef __INCLUDE_SIGNATURE_DEFINITION__
#define __INCLUDE_SIGNATURE_DEFINITION__

#include <map>
#include <string>

#include "InstructionProxy.h"
#include "Signature.h"

namespace llvm {
  class SignatureDefinition {
  public:
    static SignatureDefinition * getSignatureDefinition(LLVMContext &context);
    const Signature * getSignature(const InstructionProxy *inst);
    void addWaveform(const std::string &inst, LLVMContext &context, const Waveform *waveform);
    std::vector<InstructionProxy> matchSignature(int startIndex, int endIndex, const Signature *key);
    virtual unsigned int getPointerSize(void) const = 0;
    
    typedef std::map<InstructionProxy, const Signature *>::iterator iterator;
    typedef std::map<InstructionProxy, const Signature *>::const_iterator const_iterator;
    iterator begin(void);
    iterator end(void);
  protected:
    enum MCU {synthetic};
    static SignatureDefinition::MCU getMCU(void);
    std::map<InstructionProxy, const Signature *> signatureMap;
    const Signature *average;
  private:
    static SignatureDefinition *singleton;
  };
}

#endif
