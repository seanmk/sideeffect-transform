#!/usr/bin/env ruby

require_relative 'template'

DIR = File.expand_path(File.dirname(__FILE__))

INSTRUCTIONS = File.join(DIR, "instructions")
TEMPLATE = File.join(DIR, "SyntheticSignatureDefinition.cpp.erb")
OUT = File.join(DIR, "SyntheticSignatureDefinition.cpp")

chars = (65..90).to_a.push(32)
instructions = []
values = {}
File.readlines(INSTRUCTIONS).each do |line|
  inst = line.chomp
  instructions << inst
  values[inst] = chars[rand(27)]
end

template = Template.new(TEMPLATE, [:instructions, :values])
template.instructions = instructions
template.values = values

File.open(OUT, 'w') do |f|
  f.write(template.render)
end
