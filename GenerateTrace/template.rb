#!/usr/bin/env ruby

require 'erb'

class Template
  def initialize(template, vars = nil)
    template_contents = File.new(template).read
    @erb = ERB.new(template_contents)

    if vars.nil?
      variables = template_contents.scan(/<%=\s+([\@_$a-z]+[_0-9a-z]*)/i).uniq
      variables.flatten.each do |var|
        self.class.send(:attr_accessor, var.to_sym)
      end
    else
      vars.each do |var|
        self.class.send(:attr_accessor, var)
      end
    end
  end
  
  def render
    @erb.result(binding)
  end
end
