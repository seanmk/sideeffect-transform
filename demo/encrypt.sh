#!/bin/sh

set -x

INFILE="${1}"
OUTFILE="${INFILE}.encrypted"

./rijndael "${INFILE}" "${OUTFILE}" e 1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321
