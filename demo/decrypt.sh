#!/bin/sh

set -x

INFILE="${1}"
STRIPPED=`echo "${INFILE}" | sed 's/\.encrypted//'`
OUTFILE="${STRIPPED}.decrypted"

./rijndael "${INFILE}" "${OUTFILE}" d 1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321
