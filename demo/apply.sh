#!/bin/sh

set -x

SOURCE="${1}"
BACKUP="${1}.bak"

mv "${SOURCE}" "${BACKUP}"

~/workspace/avr-working/bin/opt -load ~/workspace/sideeffect-transform/build/ApplySynthetic/libApplySyntheticSE.so -apply-synthetic -S -o "${SOURCE}" "${BACKUP}"
