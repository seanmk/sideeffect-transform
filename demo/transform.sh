#!/bin/sh

set -x

SOURCE="${1}"
BACKUP="${1}.bak"

mv "${SOURCE}" "${BACKUP}"

KEY="${2}"

~/workspace/avr-working/bin/opt -load ~/workspace/sideeffect-transform/build/Transform/libTransformSE.so -transform-sideeffects -keyfile "${KEY}" -S -o "${SOURCE}" "${BACKUP}"
